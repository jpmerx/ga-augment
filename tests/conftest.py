from pathlib import Path

from pytest import fixture


@fixture
def datadir():
    return Path(__file__).resolve().parent / 'data'
