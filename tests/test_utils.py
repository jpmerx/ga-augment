from filecmp import cmp
from gettext import translation

import pytest

from gaaugment import APPNAME
from gaaugment.lib.transformers import DateToString, month_bre
from gaaugment.lib.utils import records2column, tdcsv2column, csv2records, column2csv, name_append2end, \
    csv_date_transform, csv_esc
from gaaugment.settings import BASE_DIR

translate = translation(APPNAME, BASE_DIR / 'locale', fallback=True)
_ = translate.gettext


def test_records2column():
    input_records = [['Amiard', 'Andre Sebastien ', '11 Juin 1823'],
                     ['Aubin', 'Victoire Melanie', '5 Novembre 1823']]
    expected_column = ['Amiard', 'Andre Sebastien', '11 Juin 1823', 'Aubin', 'Victoire Melanie', '5 Novembre 1823']
    assert expected_column == records2column(input_records)


def test_csv2records_exception():
    bad_file = 'bad_file'
    with pytest.raises(FileNotFoundError):
        csv2records(bad_file)


def test_tdcsv2column(datadir):
    expected_column = ['Amiard', 'Andre Sebastien', '11 Juin 1823', 'Aubin', 'Victoire Melanie', '5 Novembre 1823']
    assert tdcsv2column(datadir / 'td_ok1.csv') == expected_column


def test_column2csv(tmp_path, datadir):
    column = ['Amiard', 'Andre Sebastien', '11 Juin 1823', 'Aubin', 'Victoire Melanie', '5 Novembre 1823']
    column2csv(column, tmp_path / 'td_ok1_column.csv')
    assert cmp(datadir / 'td_ok1_column.csv', tmp_path / 'td_ok1_column.csv')


def test_name_append2end1():
    input_name = '/dev/ga-augment/data/archives_0001.xlsx'
    assert str(name_append2end(input_name, '_suffix')) == '/dev/ga-augment/data/archives_0001_suffix.xlsx'


def test_name_append2end2():
    input_name = '/dev/ga-augment/data/archives_0001.xlsx'
    assert str(name_append2end(input_name, '')) == '/dev/ga-augment/data/archives_0001.xlsx'


def test_csv_date_transform(tmp_path, datadir):
    csv_date_transform(in_file=datadir / 'td_scrap_in1.csv', out_file=tmp_path / 'td_scrap1_eSc.csv',
                       date_transformer=DateToString(ditto_str='id'))
    assert cmp(datadir / 'td_scrap1_eSc.csv', tmp_path / 'td_scrap1_eSc.csv')

    csv_date_transform(in_file=datadir / 'td_scrap_in2.csv', out_file=tmp_path / 'td_scrap2_eSc.csv',
                       date_transformer=DateToString(month_modifier=month_bre, ditto_str='id'))
    assert cmp(datadir / 'td_scrap2_eSc.csv', tmp_path / 'td_scrap2_eSc.csv')


def test_csv_date_transform_err(datadir, capsys):
    csv_date_transform(in_file=datadir / 'td_scrap_nodate.csv', out_file=datadir / 'td_scrap1_eSc.csv',
                       date_transformer=DateToString())
    captured = capsys.readouterr()
    assert "No 'Date'" in captured.err


def test_csv_esc(tmp_path, datadir):
    csv_esc(in_file=datadir / 'td_scrap3_eSc.csv', out_file=tmp_path / 'td_scrap3_eScriptorium.csv')
    assert cmp(datadir / 'td_scrap3_eScriptorium.csv', tmp_path / 'td_scrap3_eScriptorium.csv')
