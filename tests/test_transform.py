import datetime

from gaaugment.lib.transformers import DateToString, day_1er, month_bre, year_std


def test_td_date_to_string():
    date_td = DateToString(day_modifier=day_1er, month_modifier=month_bre, year_modifier=year_std)
    assert '1^er 8^bre 1883' == date_td.to_string(date2transform=datetime.date(1883, 10, 1))

    date_td = DateToString(day_modifier=day_1er, month_modifier=month_bre, year_modifier=year_std, ditto_str='id')
    previous_date = datetime.date(1883, 10, 1)
    assert '1^er 8^bre 1883' == date_td.to_string_ditto(previous_date=None, date2transform=datetime.date(1883, 10, 1))
    assert '2 id id' == date_td.to_string_ditto(previous_date=previous_date, date2transform=datetime.date(1883, 10, 2))
    assert '2 id id' == date_td.to_string_ditto(previous_date=previous_date, date2transform=datetime.date(1883, 10, 2))
