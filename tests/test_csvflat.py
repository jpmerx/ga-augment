import pytest
from click.testing import CliRunner

from gaaugment.csvflat import csvflat

runner = CliRunner()


def test_cvsflat_noinput():
    # noinspection PyTypeChecker
    response = runner.invoke(csvflat, ['bad_file'])
    assert response
    # csvflat('_suffix', bad_files)
    # captured = capsys.readouterr()
    # assert 'bad_file' in captured.err


def test_cvsflat_ok(datadir):
    # noinspection PyTypeChecker
    response = runner.invoke(csvflat, [datadir / 'td_ok1.csv'])
    assert response
