# Welcome to GenAuto Augmented

A set of utility programs helpful to convert indexed genealogical data to
data similar to the ones of Civil Registry images

*Examples of csv file formats in [data](tests/data) directory.*

## To install gaaugment

`pip install git+https://gitlab.com/jpmerx/ga-augment.git#egg=gaaugment`

## For files located in a Google drive 

```
from google.colab import drive
drive.mount('/content/drive')
```

## First example
Converts a csv file containing a 'Date' column into one with 'Date_eSc' column added with dates
formatted with full (French) months and **'id' string for dittos**.

```
from gaaugment.lib.utils import csv_date_transform
from gaaugment.lib.transformers import DateToString

tests_drive = '/content/drive/Shared drives/GenAuto CSA/genauto-augmente/tests/'
in_file = tests_drive + 'td_scrap_in1.csv'
out_file = tests_drive + 'td_scrap1_eSc.csv'

date_transformer = DateToString(ditto_str='id')
csv_date_transform(in_file=in_file, out_file=out_file,
                   date_transformer=date_transformer)
```


## Second example
Converts a csv file containing a 'Date' column into one with 'Date_eSc' column added with dates
formatted with **"bre" months and 'id' string for dittos**.

```from gaaugment.lib.utils import csv_date_transform
from gaaugment.lib.transformers import DateToString, month_bre

tests_drive = '/content/drive/Shared drives/GenAuto CSA/genauto-augmente/tests/'
in_file = tests_drive + 'td_scrap_in2.csv'
out_file = tests_drive + 'td_scrap2_eSc.csv'

date_transformer = DateToString(month_modifier=month_bre, ditto_str='id')
csv_date_transform(in_file=in_file, out_file=out_file,
                   date_transformer=date_transformer)
```



## Third example

Converts a csv file containing at least 'NomPrenoms', 'Date_eSc' and 'Image'
columns into a file ready to be loaded in eScriptorium.

```from gaaugment.lib.utils import csv_esc

tests_drive = '/content/drive/Shared drives/GenAuto CSA/genauto-augmente/tests/'
in_file = tests_drive + 'td_scrap3_eSc.csv'
out_file = tests_drive + 'td_scrap3_eScriptorium.csv'

csv_esc(in_file=in_file, out_file=out_file)
```