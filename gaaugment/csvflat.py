import sys

import click

from gaaugment.lib.errors import perror
from gaaugment.lib.utils import tdcsv2column, column2csv, name_append2end


@click.command()
@click.option('-o', '--suffix', default='_flat', show_default=True,
              help='Suffix for output files')
@click.argument('files', nargs=-1, type=click.Path(exists=False, dir_okay=False))
def csvflat(suffix, files):
    for file in files:
        try:
            column = tdcsv2column(file)
        except OSError as e:
            # To mimic linux errors, see https://docs.python.org/3/library/errno.html
            # TODO: move stderr to logger
            sys.stderr.write(perror(e.filename, e.errno))
            continue
        column2csv(column, name_append2end(file, suffix))


if __name__ == '__main__':
    csvflat()
