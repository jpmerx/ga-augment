# Dates modifiers

from collections import OrderedDict
from datetime import date, datetime
from typing import Callable, Optional, Sequence, KeysView


def strpdate(str_date: date) -> date:
    dt = datetime.strptime(str_date, "%d/%m/%Y")
    return date(dt.year, dt.month, dt.day)


def year_std(i_year: int) -> str:
    return str(i_year)


def month_std(i_month: int) -> str:
    months = {1: 'janvier', 2: 'février', 3: 'mars', 4: 'avril', 5: 'mai', 6: 'juin',
              7: 'juillet', 8: 'août', 9: 'septembre', 10: 'octobre', 11: 'novembre', 12: 'décembre'}
    return months[i_month]


def month_bre(i_month: int) -> str:
    months = {1: 'janvier', 2: 'février', 3: 'mars', 4: 'avril', 5: 'mai', 6: 'juin',
              7: 'juillet', 8: 'août', 9: '7^bre', 10: '8^bre', 11: '9^bre', 12: 'X^bre'}
    return months[i_month]


def day_std(i_day: int) -> str:
    return str(i_day)


def day_1er(i_day: int) -> str:
    return '1^er' if i_day == 1 else str(i_day)


class DateToString:
    """Utility class to transform dates based on Civil Register officers
    usages"""

    def to_string(self, date2transform: date) -> str:
        return (f"{self.day_modifier(date2transform.day)} {self.month_modifier(date2transform.month)} "
                f"{self.year_modifier(date2transform.year)}")

    def to_string_ditto(self, previous_date: Optional[date], date2transform: date) -> str:
        if not previous_date or self.ditto_str is None:
            return self.to_string(date2transform)
        day, month, year = date2transform.day, date2transform.month, date2transform.year
        previous_day, previous_month, previous_year = previous_date.day, previous_date.month, previous_date.year
        day_str, month_str, year_str = self.day_modifier(day), self.month_modifier(month), self.year_modifier(year)
        if year == previous_year:
            year_str = self.ditto_str
            if month == previous_month:
                month_str = self.ditto_str
        return f"{day_str} {month_str} {year_str}"

    def __init__(self, day_modifier: Callable[[int], str] = day_std, month_modifier: Callable[[int], str] = month_std,
                 year_modifier: Callable[[int], str] = year_std, ditto_str: str = None):
        self.day_modifier = day_modifier
        self.month_modifier = month_modifier
        self.year_modifier = year_modifier
        self.ditto_str = ditto_str


class RowDateTransformer:
    """Utility class to define csv file output row from input row and transformers
    for dates and names"""

    @staticmethod
    def dict_out_row(in_fieldnames: Sequence[str]) -> dict:
        out_fields = [field for field in in_fieldnames]
        out_fields.insert(out_fields.index("Date") + 1, "Date_eSc")
        return {field: None for field in out_fields}

    def __init__(self, in_fieldnames: Sequence[str], date_transformer: DateToString = DateToString()):
        self._current_row = RowDateTransformer.dict_out_row(in_fieldnames)
        self._previous_row = None
        self._date_transformer = date_transformer

    def __str__(self) -> str:
        return (f"Current row : {self._current_row}\n"
                f"Previous row: {self._previous_row}")

    def keys(self) -> KeysView:
        return self._current_row.keys()

    def same_page(self, in_row: OrderedDict):
        return False if not self._previous_row else in_row['Image'] == self._previous_row['Image'] and \
                                                    in_row['Page'] == self._previous_row['Page']

    def update(self, in_row: OrderedDict):
        previous_date = strpdate(self._previous_row["Date"]) if self.same_page(in_row) else None
        self._current_row.update(in_row,
                                 Date_eSc=self._date_transformer.to_string_ditto(previous_date=previous_date,
                                                                                 date2transform=strpdate(
                                                                                     in_row[
                                                                                         "Date"])))
        self._previous_row = self._current_row

    @property
    def current_row(self) -> dict:
        return self._current_row
