import os


def perror(s, errno):
    # Mimic https://man7.org/linux/man-pages/man3/perror.3.html
    return f"{s}: {os.strerror(errno)}\n" if s else f"{os.strerror(errno)}\n"
