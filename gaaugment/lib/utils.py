import csv
import sys
from csv import reader, writer
from pathlib import Path
from typing import TypeVar, Sequence, AnyStr

from .errors import perror
from .transformers import DateToString, RowDateTransformer

PathLike = TypeVar("PathLike", str, Path)


def records2column(records: Sequence[Sequence[AnyStr]]) -> Sequence[AnyStr]:
    """
Flatten a list of lists of strings to a list of strings
    :param records: list of lists of strings
    :return: list of strings
    """
    td_column = []
    for record in records:
        td_column.extend(record)
    return [field.strip() for field in td_column]


def csv2records(td_file: PathLike) -> Sequence[Sequence[AnyStr]]:
    records = []
    with open(td_file, newline='') as csv_file:
        td_reader = reader(csv_file, dialect='excel')
        for record in td_reader:
            records.append(record)
        return records


def tdcsv2column(td_file: PathLike) -> Sequence[AnyStr]:
    """
Return a list of fields from a csv file WITH header containing LastName, FirstNames, Date records
(the header is striped in the output)
    :param td_file: the csv file of the TD records
    :return: list of fields
    """
    records = csv2records(td_file)
    return records2column(records[1:])


def column2csv(column: Sequence[AnyStr], csv_file: PathLike):
    """
Write a list of strings to a csv file, one string per line
    :param column: the list of strings
    :param csv_file: the csv file to write in
    """
    try:
        with open(csv_file, mode='xt', newline='') as csv_file:
            csv_writer = writer(csv_file, dialect='excel')
            for row in column:
                csv_writer.writerow([row])
    except OSError as e:
        # To mimic linux errors, see https://docs.python.org/3/library/errno.html
        sys.stderr.write(perror(e.filename, e.errno))


def name_append2end(file: PathLike, str2append: AnyStr) -> PathLike:
    path = Path(file)
    return (path.parents[0] / (str(path.stem) + str2append)).with_suffix(path.suffix)


def existing_column(fieldname: str, column_fieldnames: Sequence[str], in_file: PathLike) -> bool:
    """Test if a column exists and write an error if not"""
    existing = fieldname in column_fieldnames
    if not existing:
        sys.stderr.write(f"No '{fieldname}' column in '{in_file}'\n")
    return existing


def csv_date_transform(in_file: PathLike, out_file: PathLike, date_transformer: DateToString = DateToString()):
    """Function that transform an input csv scrapped from genealogical data
    to one that contains additional columns based on images real content
    :param in_file: input csv file
    :param out_file: output csv file
    :param date_transformer: a DateToString object performing date transformations
    :return: """

    try:
        with open(in_file, newline='') as csv_in:
            csv_reader = csv.DictReader(csv_in)
            if not existing_column('Date', csv_reader.fieldnames, in_file):
                return
            try:
                with open(out_file, mode='xt', newline='') as csv_out:
                    out_row = RowDateTransformer(csv_reader.fieldnames, date_transformer=date_transformer)
                    csv_writer = csv.DictWriter(csv_out, fieldnames=out_row.keys())
                    csv_writer.writeheader()
                    for in_row in csv_reader:
                        out_row.update(in_row)
                        csv_writer.writerow(out_row.current_row)
            except OSError as e:
                sys.stderr.write(perror(e.filename, e.errno))
    except OSError as e:
        sys.stderr.write(perror(e.filename, e.errno))


def csv_esc(in_file: PathLike, out_file: PathLike):
    """Function that transform an input csv scrapped from genealogical data containing the
    fields ['NomPrenoms', 'Date_eSc', 'Image']
    to one that contains the two columns: ['eScriptorium', 'Image']. The rows are
    nom_prenoms_data_1, image1
    date_esc_data_1, image1
    nom_prenoms_data_2, image1
    date_esc_data_2, image1
    ...
    nom_prenoms_data_n, image2
    date_esc_data_n, image2
    ...
    :param in_file: input csv file
    :param out_file: output csv file
    :return: """

    try:
        with open(in_file, newline='') as csv_in:
            csv_reader = csv.DictReader(csv_in)
            if not existing_column('Date_eSc', csv_reader.fieldnames, in_file):
                return
            if not existing_column('NomPrenoms', csv_reader.fieldnames, in_file):
                return
            try:
                with open(out_file, mode='xt', newline='') as csv_out:
                    csv_writer = csv.DictWriter(csv_out, fieldnames=['eScriptorium', 'Image'])
                    csv_writer.writeheader()
                    for in_row in csv_reader:
                        csv_writer.writerow({'eScriptorium': in_row['NomPrenoms'].rstrip(), 'Image': in_row['Image']})
                        csv_writer.writerow({'eScriptorium': in_row['Date_eSc'].rstrip(), 'Image': in_row['Image']})
            except OSError as e:
                sys.stderr.write(perror(e.filename, e.errno))
    except OSError as e:
        sys.stderr.write(perror(e.filename, e.errno))
